#!/usr/bin/env bash

set -e

if [[ "$HOSTNAME" = eldavdbuild02 ]]; then
    export ASV_DIR="/data/local/avd/asv"
    export RESULTS_DIR="/net/home/h06/avd/public_html/asv"
    OWN_DIR=$(dirname $0)
    source ${ASV_DIR}/miniconda3/bin/activate
    python ${OWN_DIR}/asv.py
else
    echo "Cancelling - will only run on eldavdbuild02."
    exit 1
fi
