"""
Mixin benchmark tests.

"""

import numpy as np

from benchmarks import ARTIFICIAL_DIM_SIZE
from iris import coords


class CFVariableMixin:
    def setup(self):
        data_1d = np.zeros(ARTIFICIAL_DIM_SIZE)

        # These benchmarks are from a user perspective, so using a user-level
        # subclass of CFVariableMixin to test behaviour. AncillaryVariable is
        # the simplest so using that.
        self.cfm_proxy = coords.AncillaryVariable(data_1d)
        self.cfm_proxy.long_name = "test"

        self.metadata_dict = {
            "long_name": "new_test",
            "standard_name": "air_temperature",
            "var_name": "air_temperature",
            "units": "degrees",
            "attributes": {"a": 1}
        }

    def time_get_basic(self):
        self.cfm_proxy.long_name

    def time_set_basic(self):
        self.cfm_proxy.long_name = self.metadata_dict["long_name"]

    def time_set_standard_name(self):
        self.cfm_proxy.standard_name = self.metadata_dict["standard_name"]

    def time_set_var_name(self):
        self.cfm_proxy.var_name = self.metadata_dict["var_name"]

    def time_set_units(self):
        self.cfm_proxy.units = self.metadata_dict["units"]

    def time_set_attributes(self):
        self.cfm_proxy.attributes = self.metadata_dict["attributes"]

    def time_set_metadata(self):
        self.cfm_proxy.metadata = self.metadata_dict
