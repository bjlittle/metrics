Runnable scripts for generating derived benchmark test data.

These are here just to record it was done.
They depend on the original reduced-content FF dump file : "umglaa_pb000-theta"
This itself was produced from the large file "umglaa_pb000"
    - too big to store here
    - explained in https://metoffice.atlassian.net/browse/NGVAT-6


TO RUN
------
You need to export a data location first,
e.g.
    $ export BENCHMARK_DATA=/tmp/persistent/my_benchmark_data


ALSO
----
On the build machine, eldavdbuild02, the data is in /data/local/avd/asv/data.
This must be maintained up-to-date for the nightly runs

It is also backed up at ...
    $ itpp@vld321 $ moo ls moose:/adhoc/projects/avd/asv/data_for_nightly_tests/
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb000-theta
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb000-theta.compressed
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb000-theta.compressed.nc
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb000-theta.compressed.pp
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb000-theta.nc
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb000-theta.pp
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb000-theta.uncompressed
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb003-theta
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb003-theta.nc
    moose:/adhoc/projects/avd/asv/data_for_nightly_tests/umglaa_pb003-theta.pp
